const pollController = require("../controllers/poll.controller");
const db = require("../models");
//const fs = require('fs')
const User = db.user

exports.allAccess = (req, res) => {
    res.status(200).send("Public Content.");
  };
  
  exports.userBoard = (req, res) => {
    //return pollController.list;
    res.status(200).send("User Content");
  };
  
  exports.users = (req, res) => {
    User.find({}, function(err, users){
      if (err) {
        return res.status(500).send({message:err});
        }
      return res.status(200).json(users)
    })
  };

  exports.delete = (req, res)=>{
    //console.log(req.body.id);
    User.findByIdAndRemove(req.body.id, function(err){
      if (err)  return res.status(500).send({message:err});
      
    })
    //res.redirect('/user/' + req.user.facebookID);
    return res.status(200).send("User deleted");
  }
    
  exports.powerUserBoard = (req, res) => {
    //pollController.list;
    res.status(200).send("Power User Content.");
  };