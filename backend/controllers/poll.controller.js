var mongoose = require('mongoose');
//var Poll = require('../models/poll.model');
const db = require("../models");
//const fs = require('fs')
const Poll = db.poll;
//const Role = db.role;

exports.vote = (req, res, next) => {
    
    Poll.findOne({_id : req.body.id}, (err, doc) =>{
      let userFound = false;
      doc.options.map((option)=>{
        option.votes.map((user)=>{
          
          if(user.user === req.body.username){
            userFound = true;
          }
        })
      })
          if (err){
          return res.status(500).send({ message: err });
          //console.error(err);
        } 
        if(userFound === false){
          doc.options[req.body.index].votes.push({ 
            user : req.body.username
        });
        doc.updated = false
        doc.save((err, done)=>{
            if (err) {
              return res.status(500).send({ message: err });
            }
        });
        }
        if(userFound){
           return res.send({message:"Only one vote per user"});
          
        }else{
          return res.status(200).send({message:"Vote Succesfully"});
        }
    })
};

exports.update = (req, res) => {
  if (req.body.closed) {
    Poll.findByIdAndUpdate({_id : req.body.id}, 
      {
        updated:req.body.updated,
        closed: req.body.closed,
      }, (err, raw)=>{
        if (err) {
          return res.status(500).send({message: err});;
        }
        return res.status(200).send("Poll update succesfully");
      })
  } else {
    let option ={}
    let newOptions = []
    req.body.options.map((item, index)=>{
      option = {
        label : item.label,
        votes : []
      }
      newOptions.push(option);
    })
    Poll.findByIdAndUpdate({_id : req.body.id}, 
      {
        updated:req.body.updated,
        options:newOptions
      }, (err, raw)=>{
        if (err) {
          return res.status(500).send({message: err});;
        }
        return res.status(200).send("Poll update succesfully");
      })
  }
 }

exports.create = (req, res)=>{
    var newPoll = new Poll();
    newPoll.title = req.body.title;
    newPoll.closed = false;
    newPoll.updated = false;
    
    newPoll.author = {
        displayName: req.body.username,
        user: req.body.username
    }
    
    req.body.options.map((item, index)=>{
      var option = {
        label : item,
        votes : []
      }
      newPoll.options[index] = option;
    })
      newPoll.save(err => {
            if (err) {
                return res.status(500).send({ message: err });
            };
      })
    
    return res.status(200).send("Poll created successfully")
    //res.redirect('/user/' + req.user.facebookID);
  }

  exports.delete = (req, res)=>{
    Poll.findByIdAndRemove(req.body.id, function(err){
      if (err)  return res.status(500).send({message:err});
      
    })
    //res.redirect('/user/' + req.user.facebookID);
    return res.status(200).send("Poll deleted");
  }

  exports.list = (req, res)=>{
    Poll.find({}, function(err, polls){
      if (err) {
        return res.status(500).send({message:err});
        }
      return res.status(200).json(polls)
    })
  }