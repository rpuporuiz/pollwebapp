const dbConfig = require('./config/db.config')
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
var bcrypt = require("bcryptjs");
const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json({limit: "50mb"}));

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

const db = require("../backend/models");
const { populate } = require('./models/user.model');
const Role = db.role;
const User = db.user;

db.mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    populateRoles();
    populateUsers();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

  function populateRoles(){
    Role.estimatedDocumentCount((err, count) => {
      if (!err && count === 0) {
        new Role({
          name: "user"
        }).save(err => {
          if (err) {
            console.log("error", err);
          }
  
          console.log("added 'user' to roles collection");
        });
  
        new Role({
          name: "power_user"
        }).save(err => {
          if (err) {
            console.log("error", err);
          }
  
          console.log("added 'power_user' to roles collection");
        });
  
        new Role({
          name: "admin"
        }).save(err => {
          if (err) {
            console.log("error", err);
          }
  
          console.log("added 'admin' to roles collection");
        });
      }
    });
  }

  function populateUsers(){
    User.estimatedDocumentCount((err, count) => {
      if (!err && count === 0) {
        let rolesAdmin = ["user","power_user","admin"];
        const userAdmin = new User({
          username: "admin",
          password: bcrypt.hashSync("12345678", 8)
        })
        userAdmin.save(err => {
          if (err) {
            console.log("error", err); 
          }
          
          Role.find(
              {
                name: { $in: rolesAdmin }
              },
              (err, roles) => {
                if (err) {
                  console.log("error", err);
                }
      
                userAdmin.roles = roles.map(role => role._id);
                userAdmin.save(err => {
                  if (err) {
                    console.log("error", err);
                  }
                  //return res.send({ message: "User was registered successfully!" });
                });
              }
            );
          console.log("added 'admin' to users collection with default pass '12345678'");
        });
        let rolesPowerUser = ["user","power_user"];
        const userPowerUser = new User({
          username: "poweruser",
          password: bcrypt.hashSync("12345678", 8)
        })
        userPowerUser.save(err => {
          if (err) {
            console.log("error", err); 
          }
          
          Role.find(
              {
                name: { $in: rolesPowerUser }
              },
              (err, roles) => {
                if (err) {
                  console.log("error", err);
                }
      
                userPowerUser.roles = roles.map(role => role._id);
                userPowerUser.save(err => {
                  if (err) {
                    console.log("error", err);
                  }
                  //return res.send({ message: "User was registered successfully!" });
                });
              }
            );
          console.log("added 'poweruser' to users collection with default pass '12345678'");
        });
      }
    });
  }

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to backend application." });
});

// routes
require('../backend/routes/auth.routes')(app);
require('../backend/routes/user.routes')(app);
require('../backend/routes/poll.routes')(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});