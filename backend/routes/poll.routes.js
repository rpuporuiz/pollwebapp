var express = require('express')
var router = express.Router();

const { authJwt } = require("../middlewares");
const controller = require("../controllers/poll.controller");

module.exports = function(app) {
    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

app.post("/api/poll/vote", controller.vote)

app.put("/api/poll/update", controller.update)

app.post("/api/poll/create", controller.create)

app.post("/api/poll/delete", controller.delete)

app.get("/api/poll/list", controller.list)
}

