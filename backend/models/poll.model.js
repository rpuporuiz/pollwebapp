const mongoose = require('mongoose');

const pollSchema = mongoose.Schema({
  date : {type: Date, default: Date.now},
  title : String,
  updated : Boolean,
  closed: Boolean,
  author : {
    displayName : String,
    user : String
  },
  options : [{
    label : String,
    votes : [{
      user : String
    }]
  }]
})

module.exports = mongoose.model('Poll', pollSchema);