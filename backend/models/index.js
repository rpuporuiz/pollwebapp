const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.poll = require("./poll.model")

db.ROLES = ["user", "power_user", "admin"];

module.exports = db;