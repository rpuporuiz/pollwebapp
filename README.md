The backend is divided in to routes, middlewares, controllers and database models. In backend's config folder can be configured de DB and the authorization JWT secrets. To run the backend: 

Navigate in terminal to backend folder
### cd to/project/folder/backend
To install dependencies:
### npm install 
To run backend
### node server.js

The frontend is divided in to services and components. The services are to manage de authorization, the polls management and the users management to communicate with the server. In the .env file the port is defined. To run the frontend:

Navigate in terminal to backend folder
### cd to/project/folder/frontend
To install dependencies:
### npm install 
To run frontend
### npm start

The "admin" user has a default pass  12345678. The "poweruser" (Power_User role) has a default pass 12345678. Both users are created by default in the first run of the backend. The admins and power users are the roles who can edit the polls and the admin can edit the users too. You can see that funcionality if you are logged in as admin or poweruser. The user only can see the polls if is logged in. The polls can be created, deleted and updated by poweruser or admin. The users can be created, deleted or updated by admin. 
