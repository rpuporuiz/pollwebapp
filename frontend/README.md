This project is divided in 2 parts, backend and frontend. The backend is a NodeJS server with MongoDB. The frontend is a React app build with create-react-app as starting point.

The backend is divide in to routes, middlewares, controllers and database models.In backend's config folder can be configured de DB and authorization JWT secrets. To run the backend: 

Navigate in terminal to backend folder
# cd to/project/folder/backend
To install dependencies:
# npm install 
To run backend
# node server.js

The front end is divided in to services and components. The services are to manage de authorization, poll managment and users managment to communicate with the server. In the .env file is port is defined. To run the frontend:

Navigate in terminal to backend folder
# cd to/project/folder/frontend
To install dependencies:
# npm install 
To run frontend
# npm start






 
