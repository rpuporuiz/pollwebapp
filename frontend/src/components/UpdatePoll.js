import React, { Component } from 'react';
import { List, ListItem } from 'material-ui/List';
import PollService from '../services/poll.service';
import CreatePoll from './CreatePoll'
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';
import AuthService from '../services/auth.service'

export default class UpdatePoll extends Component{
  constructor(props){
    super(props);
    
    this.state = {
      showPoll : false,
      selectedIndex : 0,
      polls: this.props.data,
      options: [],
      title : "",
      updated: false,
      error : [],
      message: "",
      successful : false,
      closed: false
      //currentUser: AuthService.getCurrentUser()
    };
    this.handleSubmit = this.handleSubmit.bind(this)
    //this.setTitle = this.setTitle.bind(this)
    //this.optionChange = this.optionChange.bind(this)
    //this.addOption = this.addOption.bind(this)
    //this.deleteOption = this.deleteOption.bind(this)
    }

    
  handleSubmit(){
    //check fields
    var error = [];
   
    if (this.state.title === ""){
      error.push("error - Poll has no Title");
    }
    if (this.state.title > 100){
      error.push("error - Title is " + 
      (this.state.title.length - 100) + " characters too long");
    }
    
    if(this.state.options.length < 2 ){
      error.push("error - Polls must have at least 2 Options");
    }
    
    this.state.options.map(function(item, index){
      if(item === ""){
        error.push("error - Option " + (index + 1) + " has no text");
      }
      if (item.length > 100){
        error.push("error - Option " + (index + 1) + " is " + 
        (item.length - 100) + " characters too long");
      }
    });
    
    if (error.length === 0){
        
        //console.log("responsefromupdate:",this.state.options)
        PollService.updatePoll(
            this.state.polls[this.state.selectedIndex]._id,
            this.state.options,
            this.state.updated,
            this.state.closed
        ).then(res => {
            
            this.props.dataCb();
            //this.forceUpdate()
        }).catch(err => {
            if (err) throw err;
        })
    }
     else(
      this.setState({
        error : error
      })
    )
  }

  setTitle(event){
    this.setState({
      title : event.target.value
    })
  }

  optionChange(index, event){
      let option ={
        label:event.target.value,
        votes:[]
      }
    const newOptions = [
        ...this.state.options.slice(0, index),
        option,
        ...this.state.options.slice(index + 1)
      ];
      
      this.setState({
        options : newOptions
      })
     //console.log("optionsChange:",this.state.options)
  }
  addOption(event){
    const newOptions = [
      ...this.state.options.slice(),
      ""
    ]
    
    this.setState({
      options : newOptions,
      updated: true
    })
  }
  
  deleteOption(index, event){
    //console.log("deleteOptionOriginal:", this.state.options)
    const newOptions = [
      ...this.state.options.slice(0, index),
      ...this.state.options.slice(index + 1)
    ];
    //console.log("deleteOptionNew:", newOptions)
    this.setState({
      options: newOptions,
      updated : true
    })
   }

   pollListClick(e,index){
       this.setState({
           showPoll: true,
           selectedIndex: index,
           title: this.state.polls[index].title,
           options: this.state.polls[index].options
        })
   }
    render(){
      if(this.state.showPoll) {
        return (
        <div className="create">
            <div style={{marginBottom : '10px'}}>
                <h2>Update a Poll</h2>
            </div>
            <div>
                {/* {console.log("titledata:",this.props.data)} */}
                <TextField
                    style={{width : "70%", marginBottom : "10px"}}
                    floatingLabelText="Title"
                    name='title'
                    value={this.state.polls[this.state.selectedIndex].title}
                    onChange={(event) => {this.setTitle(event)}}
                />
                <br />
                {this.state.options.map(function(item, index){
                    return(
                        <div key={item._id}>
                            <button className = "button" onClick={(event) => this.deleteOption(index, event)}>
                                <i className="material-icons">delete</i>
                            </button>
                            
                            <TextField
                                name = {"Option " + (index + 1)}
                                value={item.label}
                                onChange={(event) => this.optionChange(index, event)}
                                style={{width : "70%"}}
                            />
                            <br />
                        </div>
                    )
                }, this)}
                
                <RaisedButton
                    style={{width : "35%", marginTop : '10px', marginBottom : '20px'}}
                    label="Add Option"
                    secondary={true}
                    onClick={(event) => this.addOption(event)}
                />
                <br />
                <div style={{width : "40%", margin : 'auto'}}>
                    <Checkbox style={{margin : 'auto'}}
                        label="Close Poll?"
                        checked={this.state.closed}
                        onCheck={() => {
                            this.setState({
                            closed : !this.state.closed
                        })}}
                    />
                </div>
                <RaisedButton
                    style={{width : "35%", marginTop : '10px'}}
                    label="Update Poll"
                    primary={true}
                    onClick={this.handleSubmit}
                />
            </div>
            <div>
                {this.state.error.map(function(item, index){
                    return(
                        <p key={"Attention: " + index}> 
                            {item}
                        </p>
                    )
                })}
            </div>
        </div>
        )}
      return(
        <div style={{margin : 'auto', width : '80%'}}>
            <h2 style={{textAlign : 'center'}}> Polls </h2>
            <List>
                {this.state.polls.map((poll, index) => {
                    return(
                        <div key = {poll._id}>
                            <ListItem
                                //disabled={true}
                                style = {{ borderBottom: "1px solid black"}}
                                key={index}
                                primaryText={poll.title}
                                onClick={(e)=>this.pollListClick(e,index)}
                                rightIcon = {
                                    <span>
                                        <i className="material-icons">update</i>
                                    </span>                 
                                }
                            />
                        </div>
                    )
                })}        
            </List>
           
        </div>        
        )
    }
}