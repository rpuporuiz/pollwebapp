import React, { Component, useState } from 'react';
import { List, ListItem } from 'material-ui/List';

import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton';
import PollService from '../services/poll.service';

const DeletePoll = (props) => {
  const [isOpen, setIsOpen] = useState(false)
  const [selectedIndex, setSelectedIndex] = useState(0)
  const [polls, setPolls] = useState(props.data)
  const [pollDeleted, setPollDeleted] = useState(false)
  const [initialRender, setInitialRender] = useState(true)
  const [pollsNumber, setPollsNumber] = useState(props.data.lenght)
  const [content, setContent] = useState("")

  const handleOpen = (index) => {
    setIsOpen(true)
    setSelectedIndex(index)
  };

  const handleClose = () =>{
    setIsOpen(false)
    setSelectedIndex(0)
  };

  const updateData = () => {
    PollService.list().then(res => {
      setPolls(res.data)
      setPollDeleted(true)
      setIsOpen(false)
      setSelectedIndex(0)
      setPollsNumber(pollsNumber - 1)
    }).catch(error => {
      let errorContent = (error.response &&
        error.response.data &&
        error.response.data.message) ||
        error.message ||
        error.toString() 
      setContent(errorContent)
    });
  }

  const deletePoll = () =>{
    PollService.delete(
      polls[selectedIndex]._id
    ).then(res => {
      props.dataCb();
    }).catch(err => {
      if (err) throw err;
    })
  }

  const actions = [      
    <FlatButton
      label="Cancel"
      secondary={true}
      onClick={handleClose}
    />,
    <FlatButton
      label="Delete"
      primary={true}
      onClick={deletePoll}
    />
  ]

  return(
    <div style={{margin : 'auto', width : '80%'}}>
      <h2 style={{textAlign : 'center'}}> Select a poll to delete </h2>
      <List>
        {(pollDeleted || initialRender) && polls.map((poll, index) => {
          return(
            <div key= {poll.id}>
              <ListItem
                style = {{ borderBottom: "1px solid black"}}
                key={index}
                primaryText={poll.title}
                onClick={() => handleOpen(index)}
                rightIcon = {
                  <span>                   
                    <i class="material-icons">delete</i>
                  </span>                 
                }
              />
            </div>
          )
        })}
        
        <Dialog
        title="Delete Poll"
        actions={actions}
        modal={true}
        open={isOpen}
      >
        {pollsNumber > 0 && "Are you sure you want to delete " + polls[selectedIndex].title
        }
      </Dialog>
      </List>
    </div>
  )
}

export default DeletePoll

// export default class DeletePoll extends Component{
//   constructor(props){
//     super(props);
    
//     this.state = {
//       isOpen : false,
//       selectedIndex : 0,
//       polls: this.props.data,
//       pollDeleted: false,
//       initialRender: true,
//       pollsNumber: this.props.data.lenght
//     };
//     this.handleOpen = this.handleOpen.bind(this)
//     this.handleClose = this.handleClose.bind(this)
//     this.deletePoll = this.deletePoll.bind(this)
//   }
  
//   handleOpen(index){
//     this.setState({
//       isOpen: true,
//       selectedIndex : index
//     });
//   };

//   handleClose(){
//     this.setState({
//       isOpen: false,
//       selectedIndex : 0
//     });
//   };

//   updateData(){
//     PollService.list().then(res => {
//         this.setState({
//         polls : res.data,
//         pollDeleted: true,
//         isOpen: false,
//         selectedIndex:0,
//         pollsNumber: this.state.pollsNumber - 1
//       });
//     }).catch(error => {
//       this.setState({
//         content:
//           (error.response &&
//             error.response.data &&
//             error.response.data.message) ||
//           error.message ||
//           error.toString()
//       })
//     });
//   }
  
//   deletePoll(){
//     PollService.delete(
//       this.state.polls[this.state.selectedIndex]._id
//     ).then(res => {
      
//       //this.handleClose()
//       //this.updateData();
//       //alert(res.message)
//       this.props.dataCb();
//       //this.forceUpdate()
//     }).catch(err => {
//       if (err) throw err;
//     })
//   }
  
//   render(){
//     const actions = [      
//       <FlatButton
//         label="Cancel"
//         secondary={true}
//         onClick={this.handleClose}
//       />,
//       <FlatButton
//         label="Delete"
//         primary={true}
//         onClick={this.deletePoll}
//       />
//     ]
    
    
//     return(
//       <div style={{margin : 'auto', width : '80%'}}>
//         <h2 style={{textAlign : 'center'}}> Select a poll to delete </h2>
//         <List>
//           {(this.state.pollDeleted || this.state.initialRender) && this.state.polls.map((poll, index) => {
//             return(
//               <div key= {poll.id}>
//                 <ListItem
//                   //disabled={true}
//                   style = {{ borderBottom: "1px solid black"}}
//                   key={index}
//                   primaryText={poll.title}
//                   onClick={() => this.handleOpen(index)}
//                   rightIcon = {
//                     <span>                   
//                       <i class="material-icons">delete</i>
//                     </span>                 
//                   }
//                 />
//               </div>
//             )
//           })}
          
//           <Dialog
//           title="Delete Poll"
//           actions={actions}
//           modal={true}
//           open={this.state.isOpen}
//         >
//           { this.state.pollsNumber > 0 && "Are you sure you want to delete " + this.state.polls[this.state.selectedIndex].title
//           }
//         </Dialog>
//         </List>
//       </div>
//     )
//   }
// }