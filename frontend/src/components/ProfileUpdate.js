import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import utils from '../utils';

//import { isEmail } from "validator";

import AuthService from "../services/auth.service";

//Verify the user characters between 3 and 20 
const vfullname = value => {
  if(value.length === 0) return;
  if (value.length < 5 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        The full name must be between 5 and 40 characters.
      </div>
    );
  }
};


//Regex to check the strongness of password
const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
//const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");

//To save the first password value
let passValue;
const vpassword = value => {
    passValue = value; 
    if(!strongRegex.test(value) && value.length !== 0) {
        return (
            <div className="alert alert-danger" role="alert">
              The password must contain at least: one lowercase alphabetical character, 
              one uppercase alphabetical character,
              one numeric character and 8 characters minimun.
            </div>
        );
    }
}

//To check the match of passwords
const vpasswordRepeat = value => {
    if(value !== passValue) {
        return (
            <div className="alert alert-danger" role="alert">
              The passwords must match!
            </div>
        );
    }
}

export default class ProfileUpdate extends Component {
  constructor(props) {
    super(props);
    

    this.state = {
        currentUser: AuthService.getCurrentUser(),
        fullName: '',
        newPassword: '',
        repeatPassword: '',
        avatarImg: '',
        successful: false,
        imagePreviewUrl: ''
    };
    this.onChangeFullName = this.onChangeFullName.bind(this);
    this.onChangeCurrentPassword = this.onChangeCurrentPassword.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onChangePasswordRepeat = this.onChangePasswordRepeat.bind(this);
    this.handleProfileUpdate = this.handleProfileUpdate.bind(this);
    this.onChangeAvatar = this.onChangeAvatar.bind(this)
  }

  onChangeFullName(e) {
    this.setState({
      fullName: e.target.value
    });
  }

  onChangeCurrentPassword(e) {
    this.setState({
      currentPassword: e.target.value
    });
  }

    onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  onChangePasswordRepeat(e) {
    this.setState({
      passwordRepeat: e.target.value
    });

  }

  onChangeAvatar(e){
    e.preventDefault();
  
    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        avatarImg: file,
        imagePreviewUrl: reader.result
      });
    }
    reader.readAsDataURL(file)
  }

   handleProfileUpdate(e) {
    e.preventDefault();

    this.setState({
      message: "",
      successful: false
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      let updatedUser = {
        username: AuthService.getCurrentUser().username,
        fullName: this.state.fullName,
        password: this.state.password,
        avatar: this.state.imagePreviewUrl
      }
      
      AuthService.update(updatedUser).then(
        response => {
          this.setState({
            message: response.data.message,
            successful: true
          });
          console.log(this.state.message)
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
          console.log(this.state.message)
        }
      ).then(
        () => {
          this.props.history.push("/profile");
          window.location.reload();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
  
          this.setState({
            message: resMessage
          });
        }
      )
    }
  }

  render() {
    
    return (
      
      <div className="col-md-12">
        <div className="card card-container">
          {this.state.avatarImg ? <img
            src={this.state.imagePreviewUrl}
            alt="profile-img"
            className="profile-img-card"
          />: (!this.state.currentUser.avatar.data) ?
          <img
          src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
          alt="profile-img"
          className="profile-img-card"
        /> :
          <img
          src={utils.imgDecode(this.state.currentUser.avatar.data)}
          alt="profile-img"
          className="profile-img-card"
        />}
          <Form
            onSubmit={this.handleProfileUpdate}
            ref={c => {
              this.form = c;
            }}
          >
            {!this.state.successful && (
              <div>
                <div className="form-group">
                  <label htmlFor="fullName">Full Name:</label>
                  <Input
                    type="text"
                    className="form-control"
                    name="fullName"
                    value={this.state.currentUser.fullName}
                    onChange={this.onChangeFullName}
                    validations={[vfullname]}
                />
                </div>
                <div className="form-group">
                  <label htmlFor="password">New Password</label>
                  <Input
                    type="password"
                    className="form-control"
                    name="password"
                    value={this.state.password}
                    onChange={this.onChangePassword}
                    validations={[vpassword]}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="passwordRepeat">Repeat New Password</label>
                  <Input
                    type="password"
                    className="form-control"
                    name="passwordRepeat"
                    value={this.state.passwordRepeat}
                    onChange={this.onChangePasswordRepeat}
                    validations={[vpasswordRepeat]}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="avatarUpload">Upload Avatar Image</label>
                  <Input
                    type="file"
                    className="form-control"
                    name="avatarUpload"
                    text = "Choose Avatar"
                    onChange={this.onChangeAvatar}
                    // validations={[required, vpasswordRepeat]}
                  />
                </div>

                <div className="form-group">
                  <button className="btn btn-primary btn-block">Update</button>
                </div>
              </div>
            )}

            {this.state.message && (
              <div className="form-group">
                <div
                  className={
                    !this.state.successful &&
                       "alert alert-danger"
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            )}
            
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
        </div>
      </div>
    );
  }
}