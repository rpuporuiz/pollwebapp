import React, {useState} from 'react'

const ImageUpload = () => {
  const [file, setFile] = setState('')
  const [imagePreview, setImagePreview] = setState('')

  const handleSubmit= (e) => {
    e.preventDefault();
    // TODO: do something with -> this.state.file
    //console.log('handle uploading-', this.state.file);
  }
  
  const handleImageChange = (e) => {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      setFile(file)
      setImagePreview(reader.result)
    } 
    reader.readAsDataURL(file)
  }

  let $imagePreview = null;
  if (imagePreview) {
    $imagePreview = (<img src={imagePreview} />);
  } else {
    $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
  }

  return (
    <div className="previewComponent">
      <form onSubmit={handleSubmit}>
        <input className="fileInput" 
          type="file" 
          onChange={handleImageChange} />
        <button className="submitButton" 
          type="submit" 
          onClick={handleSubmit}>Upload Image</button>
      </form>
      <div className="imgPreview">
        {$imagePreview}
      </div>
    </div>
  )
}

export default ImageUpload

// class ImageUpload extends React.Component {
//     constructor(props) {
//       super(props);
//       this.state = {file: '',imagePreviewUrl: ''};
//     }
  
//     _handleSubmit(e) {
//       e.preventDefault();
//       // TODO: do something with -> this.state.file
//       //console.log('handle uploading-', this.state.file);
//     }
  
//     _handleImageChange(e) {
//       e.preventDefault();
  
//       let reader = new FileReader();
//       let file = e.target.files[0];
  
//       reader.onloadend = () => {
//         this.setState({
//           file: file,
//           imagePreviewUrl: reader.result
//         });
//       } 
  
//       reader.readAsDataURL(file)
//     }
  
//     render() {
//       let {imagePreviewUrl} = this.state;
//       let $imagePreview = null;
//       if (imagePreviewUrl) {
//         $imagePreview = (<img src={imagePreviewUrl} />);
//       } else {
//         $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
//       }
  
//       return (
//         <div className="previewComponent">
//           <form onSubmit={(e)=>this._handleSubmit(e)}>
//             <input className="fileInput" 
//               type="file" 
//               onChange={(e)=>this._handleImageChange(e)} />
//             <button className="submitButton" 
//               type="submit" 
//               onClick={(e)=>this._handleSubmit(e)}>Upload Image</button>
//           </form>
//           <div className="imgPreview">
//             {$imagePreview}
//           </div>
//         </div>
//       )
//     }
//   }

//   export default ImageUpload