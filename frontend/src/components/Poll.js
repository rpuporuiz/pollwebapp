import React, { Component, useState } from 'react';
import { VictoryPie, VictoryTheme, VictoryTooltip, VictoryBar,VictoryChart } from 'victory';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import PollService from '../services/poll.service'
import UserService from "../services/user.service";

import { MDBProgress } from 'mdbreact';

const Poll = (props) => {
  const [selected, setSelected] = useState(null)
  const [closed, setClosed] = useState(props.data.closed)
  const [selectedOption, setSelectedOption] = useState("")
  const [poll, setPoll] = useState(props.data)
  const [votesLength, setVotesLength] = useState(0)
  const [voted, setVoted] = useState(false)
  const [content, setContent] = useState("")

  const handleOptionChange = (index) =>{
    setSelectedOption(index)
  }

  const selectOption= (index, event)=>{
     
      setSelected(index)
    
  }

  const updateData = () => {
    UserService.getUserBoard().then(res => {
      setPoll(res.data.find(x => x._id === poll._id))
      setVoted(true)
      setVotesLength(res.data.find(x => x._id === props.data._id).options[selected].votes.length)
    }).catch(error => {
      setContent((error.response &&
        error.response.data &&
        error.response.data.message) ||
        error.message ||
        error.toString())
    });
  }

  const sendVote = (index) => {
    props.processVote(index, props.data._id, props.pollIndex)
    updateData()
    props.dataCb()
 }

 let victoryFormat = props.data.options.map(option => {
  return {x : option.label, y : option.votes.length}
 });

 let pollLabel = props.data.closed ? "Poll closed": (props.hasVoted(props.pollIndex)!== null) && "Already voted"

 const totalPollVotes = () => {
  const votesInPoll = props.data.options.map(option => option.votes.length)
  .reduce((sum, value) => sum + value, 0)
  return votesInPoll;
 }


const mostVotedOption = () =>{
  let max = 0;
  props.data.options.map(option => {
    if(option.votes.length > max){
      max = option.votes.length
    }
  })
  return max
}

 const votePercent = (totalVotes, optionVotes) => {
   if(totalVotes === 0){
     return 0
   }
    return optionVotes * 100 / totalVotes
 }
 
 const barValue = (optionLength) =>{
   if(mostVotedOption()=== 0){
     return 0
   }
   return Math.round(optionLength * 100 / mostVotedOption())
 }

const barText = (optionLength) => {
  if(totalPollVotes() === 0){
    return 0
  }
  return Math.round(optionLength * 100 / totalPollVotes()) 
}

  return(
    <div>
      <div style={{textAlign : 'center'}}>
        <h2>{props.data.title}</h2>
        <h4>Created By {props.data.author.displayName}</h4>
      </div>
      <div className="pollFlex">
        <div className="pollList">
          {/* <RaisedButton 
              style = {{width : "100%", margin : "10px 20px"}}
              label={pollLabel}
              secondary={true}
              disabled={!hasSelected || props.data.closed}
              onClick={sendVote}
           /> */}
           <h2 style = {{color: "red",textAlign:"center"}}>{pollLabel}</h2>
          <Divider />

          <List>
            { props.data.options.map((option, index) => {
            return(
              <div>
                <ListItem 
                  onClick={() => sendVote(index)}
                  disabled={props.data.closed || props.hasVoted(props.pollIndex) !== null}
                  key={"option" + index}
                  primaryText={option.label}
                  secondaryText={"Votes: " + option.votes.length}
                  rightIcon={
                    <input type="radio"  checked = {"option"+ props.hasVoted(props.pollIndex) === "option" + index} disabled = {props.data.closed || props.hasVoted(props.pollIndex) !== null}/>}
                />{((props.hasVoted(props.pollIndex) !== null) || props.closed)  &&
                  <MDBProgress value={barValue(option.votes.length)} className="my-2">
                    {barText(option.votes.length)}%
                  </MDBProgress>}
              </div>
            )
            })}
              {/* {props.voted && poll.options.map((option, index) => {
                return(
                  <ListItem style = {{ borderBottom: "1px solid black"}}
                    onClick={() => selectOption(index)}
                    disabled={poll.closed}
                    key={"option" + index}
                    primaryText={option.label}
                    secondaryText={                      
                      "Votes: " + option.votes.length}
                    rightIcon={
                      <input type="radio"  checked = {"option"+ selected === "option" + index} disabled = {poll.closed} />}
                  />
                )
              })} */}
            <Divider />
          </List>
        </div>
        {/* {((props.hasVoted() !== null) || props.closed ) &&
          <div className="pollGraph">
            <VictoryPie
              labelRadius={90}
              data={victoryFormat}
              theme={VictoryTheme.material}
              labelComponent={
                <VictoryTooltip />}
            />
          </div>} */}
      </div>
    </div>
  )
}

export default Poll

// export default class Poll extends Component{
//   constructor(props){
//     super(props);
//     this.state = {
//       selected : null,
//       closed : this.props.data.closed,
//       selectedOption: "",
//       poll: this.props.data,
//       votesLength: 0,
//       voted: false,
//     }
//     //this.processVote = this.processVote.bind(this)
//     this.handleOptionChange = this.handleOptionChange.bind(this)
//     this.sendVote = this.sendVote.bind(this)
//   }
  
//   handleOptionChange (index) {
//     this.setState({
//       selectedOption: index,
      

//     });
//   }
  
//   selectOption(index, event){
//     if (this.state.selected === index){
//       this.setState({
//         selected : null,
        
//       })
//     }
//     else{
//       this.setState({
//         selected : index,
        
//       })
//     }
//   }

//   updateData(){
//     UserService.getUserBoard().then(res => {
//       //console.log("voted:",this.props.voted)
//       this.setState({
//         poll : res.data.find(x => x._id === this.state.poll._id),
//         voted : true,
//         votesLength: res.data.find(x => x._id === this.props.data._id).options[this.state.selected].votes.length
//       });
//       //console.log(this.state.votesLength)
      
//     }).catch(error => {
//       this.setState({
//         content:
//           (error.response &&
//             error.response.data &&
//             error.response.data.message) ||
//           error.message ||
//           error.toString()
//       })
//     });
//   }

 
//  sendVote(){
//     this.props.processVote(this.state.selected, this.props.data._id)
//     this.updateData()
//     this.props.dataCb()
//  }

  
//   render(){
//     // const indexMode = Number.isInteger(this.props.hasVoted) ? 
//     //   this.props.hasVoted : this.state.selected;
    
//      let victoryFormat = this.props.data.options.map(option => {
//         return {x : option.label, y : option.votes.length}
//        });

//       //  if(this.props.data._id !== this.state.poll._id){
//       //   this.setState({
//       //     voted: false
//       //   })
//       // }
//     //this.updateData()
//     //console.log("pollfromupdate:",this.state.poll)
    
//     //Booleans to assist in view rendering
//     //const hasVoted = Number.isInteger(this.props.hasVoted);
//     const hasSelected =  Number.isInteger(this.state.selected);
//     //const hasSignedIn = Boolean(this.props.user);
//     //const allowCustom = this.props.data.allowCustom;
//     let pollLabel = !this.props.data.closed ? "Vote":"This poll is closed";
//     return(
//       <div>
//         <div style={{textAlign : 'center'}}>
//           <h2>{this.props.data.title}</h2>
//           <h4>Created By {this.props.data.author.displayName}</h4>
//         </div>
//         <div className="pollFlex">
//           <div className="pollList">
//               <RaisedButton 
//                   style = {{width : "100%", margin : "10px 20px"}}
//                   label={pollLabel}
//                   secondary={true}
//                   disabled={!hasSelected || this.props.data.closed}
//                   onClick={this.sendVote}
//                />
              
//               <Divider />
//             <List>
              
//               { (!this.props.voted) && this.props.data.options.map((option, index) => {
//                 return(
//                   <ListItem style = {{ borderBottom: "1px solid black"}}
//                     onClick={event => this.selectOption(index, event)}
//                     disabled={this.props.data.closed}
//                     key={"option" + index}
//                     primaryText={option.label}
//                     secondaryText={"Votes: " + option.votes.length}
//                     rightIcon={
//                       <input type="radio"  checked = {"option"+ this.state.selected === "option" + index} disabled = {this.props.data.closed} />
//                     }
//                   />
//                   )
//               })}
//                 {this.props.voted && this.state.poll.options.map((option, index) => {
//                 return(
//                   <ListItem style = {{ borderBottom: "1px solid black"}}
//                     onClick={event => this.selectOption(index, event)}
//                     disabled={this.state.poll.closed}
//                     key={"option" + index}
//                     primaryText={option.label}
//                     secondaryText={                      
//                       "Votes: " + option.votes.length}
//                     rightIcon={
//                       <input type="radio"  checked = {"option"+ this.state.selected === "option" + index} disabled = {this.state.poll.closed} />
//                     }
//                   />
//                   )
//               })}
              
              
//               <Divider />
//             </List>
//           </div>
//           {(this.state.voted || this.props.voted || this.props.closed ) &&
//             <div className="pollGraph">
//               <VictoryPie
//                 labelRadius={90}
//                 data={victoryFormat}
//                 theme={VictoryTheme.material}
//                 labelComponent={
//                   <VictoryTooltip />
//                 }
//               />
//               {/* <VictoryChart
//                 theme={VictoryTheme.material}
//                 domainPadding={{ x: this.state.poll.options.length }}
//               >
//                 <VictoryBar horizontal
//                   style={{
//                     data: { fill: "#c43a31" }
//                   }}
//                   data={victoryFormat}
//                   labelComponent={
//                     <VictoryTooltip />
//                   }
//                 />
//               </VictoryChart> */}
//           </div>}
          
//         </div>
//       </div>
//     )
//   }
// }
