import React, { Component, useEffect, useState } from "react";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { List, ListItem } from 'material-ui/List';
import FontIcon from 'material-ui/FontIcon';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';

import UserService from "../services/user.service";
import Register from './Register';
import DeleteUser from './DeleteUser';
import UpdateUser from "./UpdateUser"
import UserStats from "./UserStats"

const BoardAdminUser = () => {
  const [content, setContent] = useState("")
  const [users, setUsers] = useState([])
  const [selected, setSelected] = useState(-2)

  useEffect(()=>{
    updateData()
  },[])

  const updateData = () =>{
    UserService.getAdminBoard().then(
      response => {
        setSelected(-2)
        setUsers(response.data)
      },
      error => {
        setContent((error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString())
      }
    );
  }

  return (
    <MuiThemeProvider>
        {content ?
         <div className="container">
            <header className="jumbotron">
              <h3>{content}</h3>
            </header>
          </div> :
        <div>
            <div className="container">
              <div className="flexHome">
                <div className="appList">
                <h2 style={{textAlign : 'center'}}> Users Options </h2>
                  <List>
                    <Divider />
                    <ListItem
                      style = {{ borderBottom: "1px solid red"}}
                      onClick={() => setSelected(-2)}
                      primaryText="Statistics"
                      rightIcon={
                        selected === -2 ?
                          <FontIcon className="fa fa-info-circle"/> :
                          <FontIcon />
                      }
                    />
                     
                      <ListItem 
                        style = {{ borderBottom: "1px solid red"}}
                        onClick={() => setSelected(-3)}
                        primaryText="Delete User"
                        rightIcon={
                          selected === -3 ?
                            <FontIcon className="fa fa-trash"/> :
                            <FontIcon />
                        }
                      />
                    
                    
                      <ListItem
                        style = {{ borderBottom: "1px solid red"}} 
                        onClick={() => setSelected(-1)}
                        primaryText="Create User"
                        rightIcon={
                          selected === -1 ?
                          <FontIcon className="fa fa-plus-circle"/> :
                          <FontIcon />
                        }
                      />
                    
                    
                      <ListItem
                        style = {{ borderBottom: "1px solid red"}}
                        onClick={() => setSelected(-4)}
                        primaryText="Update User"
                        rightIcon={
                          selected === -4 ?
                            <FontIcon className="fa fa-refresh"/> :
                            <FontIcon />
                      }
                    />
                </List>
                </div>
                <div className="appDisplay">
                  {selected === -3 && 
                    <DeleteUser
                      data={users}
                      dataCb={updateData}
                    />
                  }
                  {selected === -2 &&  
                    <UserStats 
                      data={users} 
                    /> 
                  }
                  {selected === -1 &&
                    <Register 
                      create = {true}
                      dataCb={updateData}
                    />
                  }
                  {(selected === -4) &&
                    <UpdateUser
                      data={users}
                      dataCb={updateData}
                    />
                  }
                  
                </div>
              </div>
            </div>
          </div>}
        </MuiThemeProvider>
        );
}

export default BoardAdminUser

// export default class BoardAdminUser extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       content: "",
//       users: [],
//       selected: -2
//     };
//     this.updateData = this.updateData.bind(this)
//   }

//   componentDidMount() {
//     this.updateData()
//   }

//   updateData(){
//     UserService.getAdminBoard().then(
//       response => {
//         this.setState({
//           selected: -2,
//           users: response.data
//         });
//       },
//       error => {
//         this.setState({
//           content:
//             (error.response &&
//               error.response.data &&
//               error.response.data.message) ||
//             error.message ||
//             error.toString()
//         });
//       }
//     );
//   }

//   render() {
//     return (
// <MuiThemeProvider>
//     {this.state.content ?
//      <div className="container">
//         <header className="jumbotron">
//           <h3>{this.state.content}</h3>
//         </header>
//       </div> :
//     <div>
//         <div className="container">
//           <div className="flexHome">
//             <div className="appList">
//             <h2 style={{textAlign : 'center'}}> Users Options </h2>
//               <List>
//                 <Divider />
//                 <ListItem
//                   style = {{ borderBottom: "1px solid red"}}
//                   onClick={() => this.setState({selected : -2})}
//                   primaryText="Statistics"
//                   rightIcon={
//                     this.state.selected === -2 ?
//                     <FontIcon className="fa fa-info-circle"/> :
//                     <FontIcon />
//                   }
//                 />
                 
//                   <ListItem 
//                     style = {{ borderBottom: "1px solid red"}}
//                     onClick={() => this.setState({selected : -3})}
//                     primaryText="Delete User"
//                     rightIcon={
//                       this.state.selected === -3 ?
//                       <FontIcon className="fa fa-user"/> :
//                       <FontIcon />
//                     }
//                   />
                
                
//                   <ListItem
//                     style = {{ borderBottom: "1px solid red"}} 
//                     onClick={() => this.setState({selected : -1})}
//                     primaryText="Create User"
//                     rightIcon={
//                       this.state.selected === -1 ?
//                       <FontIcon className="fa fa-plus-circle"/> :
//                       <FontIcon />
//                     }
//                   />
                
                
//                   <ListItem
//                     style = {{ borderBottom: "1px solid red"}}
//                     onClick={() => this.setState({selected : -4})}
//                     primaryText="Update User"
//                     rightIcon={
//                       this.state.selected === -4 ?
//                       <FontIcon className="fa fa-info-circle"/> :
//                       <FontIcon />
//                   }
//                 />
//             </List>
//             </div>
//             <div className="appDisplay">
//               {this.state.selected === -3 && 
//                 <DeleteUser
//                   data={this.state.users}
//                   dataCb={this.updateData}
//                 />
//               }
//               {this.state.selected === -2 &&  
//                 <UserStats 
//                   data={this.state.users} 
//                 /> 
//               }
//               {this.state.selected === -1 &&
//                 <Register 
//                   create = {true}
//                   dataCb={this.updateData}
//                 />
//               }
//               {(this.state.selected === -4) &&
//                 <UpdateUser
//                   data={this.state.users}
//                   dataCb={this.updateData}
//                 />
//               }
              
//             </div>
//           </div>
//         </div>
//       </div>}
//     </MuiThemeProvider>
//     );
//   }
// }