import React, { Component, useState } from 'react';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import PollService from '../services/poll.service'
import AuthService from '../services/auth.service'

const CreatePoll = (props) =>{
  const [options, setOptions] = useState(["",""])
  const [title, setTitle] = useState("")
  const [error, setError] = useState([])
  const [message, setMessage] = useState("")
  const [successful, setSuccessful] = useState(false)
  const [currentUser, setCurrentUser] = useState(AuthService.getCurrentUser())

  const handleSubmit = () => {
    //check fields
    var error = [];
    if (title === ""){
      error.push("error - Poll has no Title");
    }
    if (title > 100){
      error.push("error - Title is " + 
      (title.length - 100) + " characters too long");
    }
    if(options.length < 2 ){
      error.push("error - Polls must have at least 2 Options");
    }
    options.map(function(item, index){
      if(item === ""){
        error.push("error - Option " + (index + 1) + " has no text");
      }
      if (item.length > 100){
        error.push("error - Option " + (index + 1) + " is " + 
        (item.length - 100) + " characters too long");
      }
    });
    if (error.length === 0){
      PollService.createPoll(
          title,
          options
      ).then(
        response => {
          setSuccessful(true)
          props.dataCb()
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          
          setSuccessful(false)
          setMessage(resMessage)
        }
      )
    }else(
      setError(error)
    )
  }

  const onChangeTitle = (event)=>{
    setTitle(event.target.value)
  }
  
  const optionChange= (index, event) =>{
    const newOptions = [
        ...options.slice(0, index),
        event.target.value,
        ...options.slice(index + 1)
      ];
      setOptions(newOptions)
  }

  const addOption = (event) =>{
    const newOptions = [
      ...options.slice(),""
    ]
    setOptions(newOptions)
  }

  const deleteOption= (index, event)=>{
    const newOptions = [
      ...options.slice(0, index),
      ...options.slice(index + 1)
    ];
    setOptions(newOptions)
   }

   return(
    <div className="create">
      <div style={{marginBottom : '10px'}}>
        <h2>Create A Poll</h2>
      </div>
      <div>
        <TextField
          style={{width : "70%", marginBottom : "10px"}}
          floatingLabelText="Title"
          name='title'
          value={title}
          onChange={onChangeTitle}
        />
        <br />
        {options.map(function(item, index){
          return(
            <div key={"option" + index}>
              <button className = "button" onClick={(event)=>deleteOption(index)}>
                <i class="material-icons">delete</i>
              </button>
              <TextField
                hintText={"Option " + (index + 1)}
                value={options[index]}
                onChange={(event) => optionChange(index,event)}
                style={{width : "70%"}}
              />
              <br />
            </div>
        )}, this)}
        <RaisedButton
          style={{width : "35%", marginTop : '10px', marginBottom : '20px'}}
          label="Add Option"
          secondary={true}
          onClick={addOption}
        />
        <br />
        <RaisedButton
          style={{width : "35%", marginTop : '10px'}}
          label="Publish Poll"
          primary={true}
          onClick={handleSubmit}
        />
      </div>
      <div>
        {error.map(function(item, index){
          return(
            <p key={"error" + index}> 
              {item}
            </p>
          )
        })}
      </div>
    </div>
  )
}

export default CreatePoll

// export default class CreatePoll extends Component {
//   constructor(props){
//     super(props)
//     this.state = {
//       options : ["", ""],
//       title : "",
//       error : [],
//       message: "",
//       successful : false,
//       currentUser: AuthService.getCurrentUser()
//     }
//     this.handleSubmit = this.handleSubmit.bind(this)
//     this.deleteOption = this.deleteOption.bind(this)
//   }
  
//   handleSubmit(){
//     //check fields
//     var error = [];
    
   
//     if (this.state.title === ""){
//       error.push("error - Poll has no Title");
//     }
//     if (this.state.title > 100){
//       error.push("error - Title is " + 
//       (this.state.title.length - 100) + " characters too long");
//     }
//     //console.log("lenght:",this.state.options.length)
//     if(this.state.options.length < 2 ){
//       error.push("error - Polls must have at least 2 Options");
//     }
    
//     this.state.options.map(function(item, index){
//       if(item === ""){
//         error.push("error - Option " + (index + 1) + " has no text");
//       }
//       if (item.length > 100){
//         error.push("error - Option " + (index + 1) + " is " + 
//         (item.length - 100) + " characters too long");
//       }
//     });
    
//     if (error.length === 0){
//       //console.log("pollservice call",this.state.currentUser)
//       PollService.createPoll(
//           this.state.title,
//           this.state.options
//       ).then(
//         response => {
//           //console.log("responsedata",response.data)
//           this.setState({
//             //message: response.data.message,
//             successful: true
//           });
//           this.props.dataCb()
//         },
//         error => {
//           const resMessage =
//             (error.response &&
//               error.response.data &&
//               error.response.data.message) ||
//             error.message ||
//             error.toString();

//           this.setState({
//             successful: false,
//             message: resMessage
//           })
          
//         }
//       )
//     }else(
//       this.setState({
//         error : error
//       })
//     )
//   }
  
//   onChangeTitle(event){
//     this.setState({
//       title : event.target.value
//     })
//   }
  
//   optionChange(index, event){
//     const newOptions = [
//         ...this.state.options.slice(0, index),
//         event.target.value,
//         ...this.state.options.slice(index + 1)
//       ];
      
//       this.setState({
//         options : newOptions
//       })
//   }
  
//    addOption(event){
//     const newOptions = [
//       ...this.state.options.slice(),
//       ""
//     ]
    
//     this.setState({
//       options : newOptions
//     })
//   }
  
//   deleteOption(index, event){
//     const newOptions = [
//       ...this.state.options.slice(0, index),
//       ...this.state.options.slice(index + 1)
//     ];
//     this.setState({
//       options: newOptions
//     })
//    }
  
//   render(){
//     return(
//         <div className="create">
//           <div style={{marginBottom : '10px'}}>
//             <h2>Create A Poll</h2>
//           </div>
//           <div>
//             <TextField
//               style={{width : "70%", marginBottom : "10px"}}
//               floatingLabelText="Title"
//               name='title'
//               value={this.state.title}
//               onChange={(event) => {this.onChangeTitle(event)}}
//             />
//             <br />
//             {this.state.options.map(function(item, index){
//               return(
//                 <div key={"option" + index}>
//                   <button className = "button" onClick={(event) => this.deleteOption(index, event)}>
//                     <i class="material-icons">delete</i>
//                   </button>
//                   <TextField
//                     hintText={"Option " + (index + 1)}
//                     value={this.state.options[index]}
//                     onChange={(event) => this.optionChange(index, event)}
//                     style={{width : "70%"}}
//                   />
                  
//                   <br />
//                 </div>
//             )}, this)}
//             <RaisedButton
//               style={{width : "35%", marginTop : '10px', marginBottom : '20px'}}
//               label="Add Option"
//               secondary={true}
//               onClick={(event) => this.addOption(event)}
//             />
//             <br />
//             {/* <div style={{width : "35%", margin : 'auto'}}>
//               <Checkbox 
//                 label="Allow Custom User Options"
//                 checked={this.state.allowCustom}
//                 onCheck={() => {
//                   this.setState({
//                     allowCustom : !this.state.allowCustom
//                   })}
//                 }
//               />
//             </div> */}
//             <RaisedButton
//               style={{width : "35%", marginTop : '10px'}}
//               label="Publish Poll"
//               primary={true}
//               onClick={this.handleSubmit}
//             />
//           </div>
//           <div>
//             {this.state.error.map(function(item, index){
//               return(
//                 <p key={"error" + index}> 
//                   {item}
//                 </p>
//               )
//             })}
//           </div>
//         </div>
//     )
//   }
// }