import React, { Component, useEffect, useState } from "react";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { List, ListItem } from 'material-ui/List';
import FontIcon from 'material-ui/FontIcon';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';

import Poll from './Poll';
import Stats from "./Stats"
import Create from './CreatePoll';
import DeletePoll from './DeletePoll';
import UpdatePoll from "./UpdatePoll"

import UserService from "../services/user.service";
import AuthService from "../services/auth.service";

const BoardPowerUser = () => {
  const [polls, setPolls] = useState([])
  const [selected, setSelected] = useState(-2)
  const [content, setContent] = useState("")

  useEffect(()=>{
    updateData()
  },[])

  const updateData = () =>{
    UserService.getPowerUserBoard().then(res => {
      setPolls(res.data)
      setSelected(-2)
    }).catch(error => {
      setContent((error.response &&
        error.response.data &&
        error.response.data.message) ||
        error.message ||
        error.toString())
    });
  }

  return(
    <MuiThemeProvider>
    {content ?
     <div className="container">
        <header className="jumbotron">
          <h3>{content}</h3>
        </header>
      </div> :
    <div>
        <div className="container">
          <div className="flexHome">
            <div className="appList">
            <h2 style={{textAlign : 'center'}}> Polls Options </h2>
              <List>
                <Divider />
                <ListItem
                  style = {selected === -2 && { backgroundColor: "lightblue"}}
                  onClick={() => setSelected(-2)}
                  primaryText="Statistics"
                  rightIcon={
                    selected === -2 ?
                    <FontIcon className="fa fa-info-circle"/> :
                    <FontIcon />
                  }
                />
                 
                  <ListItem 
                    style = {selected === -3 && { backgroundColor: "lightblue"}}
                    onClick={() => setSelected(-3)}
                    primaryText="Delete Poll"
                    rightIcon={
                      selected === -3 ?
                      <FontIcon className="fa fa-trash"/> :
                      <FontIcon />
                    }
                  />

                  <ListItem
                    style = {selected === -1 && { backgroundColor: "lightblue"}} 
                    onClick={() => setSelected(-1)}
                    primaryText="Create Poll"
                    rightIcon={
                      selected === -1 ?
                      <FontIcon className="fa fa-plus-circle"/> :
                      <FontIcon />
                    }
                  />

                  <ListItem
                   style = {selected === -4 && { backgroundColor: "lightblue"}}
                    onClick={() => setSelected(-4)}
                    primaryText="Update Poll"
                    rightIcon={
                      selected === -4 ?
                      <FontIcon className="fa fa-refresh"/> :
                      <FontIcon />
                  }
                />
            </List>
            </div>
            <div className="appDisplay">
              {selected === -3 && 
                <DeletePoll
                  data={polls}
                  dataCb={updateData}
                />
              }
              {selected === -2 &&  
                <Stats 
                  data={polls} 
                /> 
              }
              {selected === -1 &&
                <Create 
                  dataCb={updateData}
                />
              }
              {(selected === -4) &&
                <UpdatePoll
                  data={polls}
                  dataCb={updateData}
                />
              }
            </div>
          </div>
        </div>
      </div>}
    </MuiThemeProvider>
  )
}

export default BoardPowerUser

// export default class BoardPowerUser extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       polls : [],
//       selected : -2,
//       currentUser: AuthService.getCurrentUser(),
      
//     }
//     //this.hasVoted = this.hasVoted.bind(this)
//     this.updateData = this.updateData.bind(this)
    
//   }

//   componentDidMount() {
//      this.updateData()
//   }

//   updateData(){
//     UserService.getPowerUserBoard().then(res => {
//       this.setState({
//         polls : res.data,
//         selected : -2
//       });
//     }).catch(error => {
//       //console.log("error:",error.response.data )
//       this.setState({
//         content:
//           (error.response &&
//             error.response.data &&
//             error.response.data.message) ||
//           error.message ||
//           error.toString()
//       })
//     });
//   }

//   // hasVoted(){
//   //   var votedIndex = null;
//   //  if (this.state.currentUser.username){
//   //     this.state.polls[this.state.selected].options.map((option, index) => {
//   //       option.votes.map(vote => {
//   //         if (vote.username === this.state.currentUser.username){
//   //           votedIndex = index;
//   //         }
//   //       })
//   //     })
//   //   }
//   //   return(votedIndex);
//   // }

//   render() {
//   //     const yourPolls = this.state.polls.filter((poll) => {
//   //     return (poll.author.username === this.state.currentUser.username)
//   //  })

//    return(
//     <MuiThemeProvider>
//     {this.state.content ?
//      <div className="container">
//         <header className="jumbotron">
//           <h3>{this.state.content}</h3>
//         </header>
//       </div> :
//     <div>
//         <div className="container">
//           <div className="flexHome">
//             <div className="appList">
//             <h2 style={{textAlign : 'center'}}> Polls Options </h2>
//               <List>
//                 <Divider />
//                 <ListItem
//                   style = {{ borderBottom: "1px solid red"}}
//                   onClick={() => this.setState({selected : -2})}
//                   primaryText="Statistics"
//                   rightIcon={
//                     this.state.selected === -2 ?
//                     <FontIcon className="fa fa-info-circle"/> :
//                     <FontIcon />
//                   }
//                 />
                 
//                   <ListItem 
//                     style = {{ borderBottom: "1px solid red"}}
//                     onClick={() => this.setState({selected : -3})}
//                     primaryText="Delete Poll"
//                     rightIcon={
//                       this.state.selected === -3 ?
//                       <FontIcon className="fa fa-user"/> :
//                       <FontIcon />
//                     }
//                   />
                
                
//                   <ListItem
//                     style = {{ borderBottom: "1px solid red"}} 
//                     onClick={() => this.setState({selected : -1})}
//                     primaryText="Create Poll"
//                     rightIcon={
//                       this.state.selected === -1 ?
//                       <FontIcon className="fa fa-plus-circle"/> :
//                       <FontIcon />
//                     }
//                   />
                
                
//                   <ListItem
//                     style = {{ borderBottom: "1px solid red"}}
//                     onClick={() => this.setState({selected : -4})}
//                     primaryText="Update Poll"
//                     rightIcon={
//                       this.state.selected === -4 ?
//                       <FontIcon className="fa fa-info-circle"/> :
//                       <FontIcon />
//                   }
//                 />
//             </List>
//             </div>
//             <div className="appDisplay">
//               {this.state.selected === -3 && 
//                 <DeletePoll
//                   data={this.state.polls}
//                   dataCb={this.updateData}
//                 />
//               }
//               {this.state.selected === -2 &&  
//                 <Stats 
//                   data={this.state.polls} 
//                 /> 
//               }
//               {this.state.selected === -1 &&
//                 <Create 
//                   dataCb={this.updateData}
//                 />
//               }
//               {(this.state.selected === -4) &&
//                 <UpdatePoll
//                   data={this.state.polls}
//                   dataCb={this.updateData}
//                 />
//               }
              
//             </div>
//           </div>
//         </div>
//       </div>}
//     </MuiThemeProvider>
        
      
//   )
//     // return (
//     //   <div className="container">
//     //     <header className="jumbotron">
//     //       <h3>{this.state.content}</h3>
//     //     </header>
//     //   </div>
//     // );
//   }
// }