import React, { Component } from 'react';
import { List, ListItem } from 'material-ui/List';
import Register from './Register'



 class UpdateUser extends Component{
  constructor(props){
    super(props);
    
    this.state = {
      showUser : false,
      selectedIndex : 0,
      users: this.props.data,
      message: "",
      //currentUser: AuthService.getCurrentUser()
    };
 }

 componentDidMount(){
    //this.props.dataCb()
 }
   
 userListClick(e,index){
       this.setState({
           showUser: true,
           selectedIndex: index,
        })
   }
    render(){
        if(this.state.showUser) {
              return (
                <Register 
                    user = {this.state.users[this.state.selectedIndex]}
                    create = {false}
                    update = {true}
                />
            )}
      return(
        <div style={{margin : 'auto', width : '80%'}}>
        {/* {this.state.showUser &&
        <div>
            <Register 
                user = {this.state.users[this.state.selectedIndex]}
                create = {false}
                update = {true}
            />
        </div>} */}
            <h2 style={{textAlign : 'center'}}> Select User to Update </h2>
            <List>
                {this.state.users.map((user, index) => {
                    return(
                        <div key = {user._id}>
                            <ListItem
                                //disabled={true}
                                style = {{ borderBottom: "1px solid black"}}
                                key={index}
                                primaryText={user.username}
                                onClick={(e)=>this.userListClick(e,index)}
                                rightIcon = {
                                    <span>
                                        <i className="material-icons">update</i>
                                    </span>                 
                                }
                            />
                        </div>
                    )
                })}        
            </List>
        </div>
        )
    }
}

export default UpdateUser