import React, { Component, useState, useEffect } from "react";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { List, ListItem } from 'material-ui/List';
import FontIcon from 'material-ui/FontIcon';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import { Link } from "react-router-dom";
import Poll from './Poll';
import Stats from "./Stats"
import Create from './CreatePoll';
import DeletePoll from './DeletePoll';
import UpdatePoll from "./UpdatePoll"

import UserService from "../services/user.service";
import AuthService from "../services/auth.service";
import PollService from "../services/poll.service";

const BoardUser = () =>{
  const [polls, setPolls] = useState([])
  const [selected, setSelected] = useState(-2)
  const [currentUser, setCurrentUser] = useState(AuthService.getCurrentUser())
  const [succesfull, setSuccesfull] = useState(false)
  const [content, setContent] = useState("")

  useEffect(()=>{
    updateData()
  },[])

  // useEffect(()=>{
  //   updateData()
  // },[selected])

  const updateData = () =>{
    UserService.getUserBoard().then(res => {
      setPolls(res.data)
      //setSelected(selected)
    }).catch(error => {
      setContent((error.response &&
        error.response.data &&
        error.response.data.message) ||
        error.message ||
        error.toString())
    });
  }

  const processVote = (selectedOption, id, pollIndex)=> {
    PollService.vote(
      selectedOption, 
      id
      ).then(res => {
        //this.updateData()
        setSuccesfull(true)
        setSelected(pollIndex)
    }).catch(err => {
      if (err) throw err;
    })
  }

  const hasVoted = (index) => {
    var votedIndex = null;
   if (currentUser.username){
      polls[index].options.map((option, index) => {
        option.votes.map(vote => {
          if (vote.user === currentUser.username){
            votedIndex = index;
          }
        })
      })
    }
    return(votedIndex);
  }

  return(
    <MuiThemeProvider>
      {content ?
     <div className="container">
        <header className="jumbotron">
          <h3>{content}</h3>
        </header>
      </div> :
    <div>
        <div className="container">
          <div className="flexHome">
            <div className="appList">
            <Link to={"/pollsTile"} className="nav-link">
                Tile View
            </Link>
              <List>
                <Divider />
                <ListItem
                  style = {selected === -2 && { backgroundColor: "lightblue"}}
                  onClick={() => {setSelected(-2); setSuccesfull(false)}}
                  primaryText="Statistics"
                  rightIcon={
                    selected === -2 ?
                      <FontIcon className="fa fa-info-circle"/> :
                      <FontIcon />
                  }
                />
                
                <Divider />
                <Subheader style = {{backgroundColor: "lightgray"}}>
                  <strong>POLLS</strong>
                </Subheader>
                <Divider />
                {polls.map(function(item, index){
                  return(
                    <ListItem 
                      style = {selected === index && { backgroundColor: "lightblue"}}
                      onClick={()=>{setSelected(index); setSuccesfull(false)}} 
                      key={"poll" + index}
                      primaryText={item.title}
                      secondaryText={item.author.displayName}
                      rightIcon={
                        item.updated && (hasVoted(index) === null)?
                        // <span>                   
                        //   <p>Updated</p>
                        // </span> 
                        <span>                   
                          <i className="material-icons">updated</i>
                        </span> :
                        <div></div>
                      }
                    />
                    
                )}, this)}
              </List>
            </div>
            <div className="appDisplay">
              {selected === -3 && 
                <DeletePoll
                  data={polls}
                  dataCb={updateData}
                />
              }
              {selected === -2 &&  
                <Stats 
                  data={polls} 
                /> 
              }
              {selected === -1 &&
                <Create 
                  dataCb={updateData}
                />
              }
              {(selected === -4) &&
                <UpdatePoll
                  data={polls}
                  dataCb={updateData}
                />
              }
              {selected >= 0 &&
              
                <Poll 
                  data={polls[selected]}
                  user={currentUser.username}
                  closed={polls[selected].closed}
                  dataCb={updateData}
                  pollIndex = {selected}
                  voted = {succesfull}
                  processVote = {processVote}
                  hasVoted = {hasVoted}
                />
              }
            </div>
          </div>
        </div>
      </div>}
    </MuiThemeProvider>
  )
}

export default BoardUser

// export default class BoardUser extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       polls : [],
//       selected : -2,
//       currentUser: AuthService.getCurrentUser(),
//       succesfull: false,
//       content: ""
//     }
//     //this.hasVoted = this.hasVoted.bind(this)
//     this.updateData = this.updateData.bind(this)
//     this.processVote = this.processVote.bind(this)
    
//   }

//   componentDidMount() {
//      this.updateData()
//   }

//   updateData(){
//     UserService.getUserBoard().then(res => {
//       this.setState({
//         polls : res.data,
//         selected : this.state.selected
//       });
//     }).catch(error => {
//       this.setState({
//         content:
//           (error.response &&
//             error.response.data &&
//             error.response.data.message) ||
//           error.message ||
//           error.toString()
//       })
//     });
//   }

//   processVote(selectedOption, id){
//     //console.log("polls:",this.state.polls)
//     PollService.vote(
//       selectedOption, 
//       id
//       ).then(res => {
//         //this.updateData()
//         this.setState({
//           succesfull:true
//         })
//         //console.log("props:",this.props)
//         alert(res.message)
       
//         //this.forceUpdate();
//     }).catch(err => {
//       if (err) throw err;
//     })
  
// }

//   // hasVoted(){
//   //   var votedIndex = null;
//   //  if (this.state.currentUser.username){
//   //     this.state.polls[this.state.selected].options.map((option, index) => {
//   //       option.votes.map(vote => {
//   //         if (vote.username === this.state.currentUser.username){
//   //           votedIndex = index;
//   //         }
//   //       })
//   //     })
//   //   }
//   //   return(votedIndex);
//   // }

//   render() {
//   //     const yourPolls = this.state.polls.filter((poll) => {
//   //     return (poll.author.username === this.state.currentUser.username)
//   //  })

//    return(
//     <MuiThemeProvider>
//       {this.state.content ?
//      <div className="container">
//         <header className="jumbotron">
//           <h3>{this.state.content}</h3>
//         </header>
//       </div> :
//     <div>
//         <div className="container">
//           <div className="flexHome">
//             <div className="appList">
//             <Link to={"/pollsTile"} className="nav-link">
//                 Tile View
//             </Link>
//               <List>
//                 <Divider />
//                 <ListItem
//                   style = {{ borderBottom: "1px solid red"}}
//                   onClick={() => this.setState({selected : -2,succesfull:false})}
//                   primaryText="Statistics"
//                   rightIcon={
//                     this.state.selected === -2 ?
//                     <FontIcon className="fa fa-info-circle"/> :
//                     <FontIcon />
//                   }
//                 />
                
//                 <Divider />
//                 <Subheader style = {{backgroundColor: "lightgray"}}>
//                   <strong>POLLS</strong>
//                 </Subheader>
//                 <Divider />
//                 {this.state.polls.map(function(item, index){
//                   return(
//                     <ListItem 
//                       style = {{ borderBottom: "1px solid black"}}
//                       onClick={()=>this.setState({selected:index,succesfull:false})} 
//                       key={"poll" + index}
//                       primaryText={item.title}
//                       secondaryText={item.author.displayName}
//                       rightIcon={
//                         item.updated ?
//                         // <span>                   
//                         //   <p>Updated</p>
//                         // </span> 
//                         <span>                   
//                           <i className="material-icons">updated</i>
//                         </span> :
//                         <div></div>
//                       }
//                     />
                    
//                 )}, this)}
//               </List>
//             </div>
//             <div className="appDisplay">
//               {this.state.selected === -3 && 
//                 <DeletePoll
//                   data={this.state.polls}
//                   dataCb={this.updateData}
//                 />
//               }
//               {this.state.selected === -2 &&  
//                 <Stats 
//                   data={this.state.polls} 
//                 /> 
//               }
//               {this.state.selected === -1 &&
//                 <Create 
//                   dataCb={this.updateData}
//                 />
//               }
//               {(this.state.selected === -4) &&
//                 <UpdatePoll
//                   data={this.state.polls}
//                   dataCb={this.updateData}
//                 />
//               }
//               {this.state.selected >= 0 &&
                
//                 <Poll 
//                   data={this.state.polls[this.state.selected]}
//                   user={this.state.currentUser.username}
//                   closed={this.state.polls[this.state.selected].closed}
//                   dataCb={this.updateData}
//                   pollIndex = {this.state.selected}
//                   voted = {this.state.succesfull}
//                   processVote = {this.processVote}
//                   hasVote = {this.hasVoted}
//                 />
//               }
//             </div>
//           </div>
//         </div>
//       </div>}
//     </MuiThemeProvider>
        
      
//   )
//     // return (
//     //   <div className="container">
//     //     <header className="jumbotron">
//     //       <h3>{this.state.content}</h3>
//     //     </header>
//     //   </div>
//     // );
//   }
// }