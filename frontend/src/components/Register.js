import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Select from "react-validation/build/select";
import CheckButton from "react-validation/build/button";
import AuthService from "../services/auth.service";
import { Route , withRouter} from 'react-router-dom';

//check for requiered field and alert to use it 
const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

//Verify the user characters between 3 and 20 
const vusername = value => {
  if (value.length < 3 || value.length > 20) {
    return (
      <div className="alert alert-danger" role="alert">
        The username must be between 3 and 20 characters.
      </div>
    );
  }
};

//Regex to check the strongness of password
const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
//const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");

//To save the first password value
let passValue;
const vpassword = value => {
    passValue = value; 
    if(!strongRegex.test(value) && value.length !== 0) {
        return (
            <div className="alert alert-danger" role="alert">
              The password must contain at least: one lowercase alphabetical character, 
              one uppercase alphabetical character,
              one numeric character and 8 characters minimun.
            </div>
        );
    }
}

//To check the match of passwords
const vpasswordRepeat = value => {
    if(value !== passValue) {
        return (
            <div className="alert alert-danger" role="alert">
              The passwords must match!
            </div>
        );
    } 
}

class Register extends Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onChangePasswordRepeat = this.onChangePasswordRepeat.bind(this);
    this.onChangeRole = this.onChangeRole.bind(this)

    this.state = {
      username: "",
      password: "",
      passwordRepeat: "",
      successful: false,
      message: "",
      create: false,
      role: "user",
      user: this.props.user,
      update: this.props.update
    };
  }

  componentDidMount(){
    this.props.create && 
    this.setState({
      create: true
    })
  }
    
  

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

    onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  onChangeRole(e) {
    this.setState({
      role: e.target.value
    });
  }

  onChangePasswordRepeat(e) {
    this.setState({
      passwordRepeat: e.target.value
    });

  }

   async handleRegister(e) {
    e.preventDefault();
    
    this.setState({
      message: "",
      successful: false
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      if(!this.state.update){
        await this.register()   
      }
      else {
        this.updateUser()
      }
        
     }
  }

  buildRoles(){
    if(this.state.role === "user"){
      return ["user"]
    }
    if(this.state.role === "power_user"){
      return ["user", "power_user"]
    }
    return ["user", "power_user", "admin"]
  }

  async register(){
    await AuthService.register(
      this.state.username,
      this.state.password,
      this.buildRoles()
    ).then(
      response => {
        this.setState({
          message: response.data.message,
          
        });
        //console.log("registerOK",response.data)
        this.props.dataCb()
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          successful: false,
          message: resMessage
        })
        
      }
    )
  }

  updateUser(){
    let rolesNew = this.buildRoles()
    let updatedUser = {
      username: this.state.user.username,
      password: this.state.password,
      roles: rolesNew
    }

    AuthService.update(updatedUser).then(
      response => {
        this.setState({
          message: response.data.message,
          successful: true
        });
        //console.log(this.state.message)
        //this.props.dataCb()
        
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          successful: false,
          message: resMessage
        });
        console.log(this.state.message)
      }
    ).then(
      () => {
        this.props.history.push("/admin");
        window.location.reload();
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          message: resMessage
        });
      }
    )
  }

  roleSelected(){
    if(this.state.user.roles.length === 1){
      return "user"
    }
    if(this.state.user.roles.length === 2){
      return "power_user"
    }
    return "admin"
  } 

  render() {
    //const { successful } = this.state;
    
    return (
      <div className="col-md-12">
        <div className="card card-container">
          {(!this.state.create && !this.state.update) &&
            <img
            src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
            alt="profile-img"
            className="profile-img-card"
          />}

          {this.state.create &&
            <h2 style={{textAlign : 'center'}}> Create New User</h2>
          }
          
          
          {this.state.update &&
            <h2 style={{textAlign : 'center'}}> Update {this.state.user.username}</h2>
          }

          <Form
            onSubmit={this.handleRegister}
            ref={c => {
              this.form = c;
            }}
          >
            {!this.state.successful && 
              <div>
                <div className="form-group">
                  <label htmlFor="username">Username</label>
                  {this.state.update ?
                    <Input
                    type="text"
                    className="form-control"
                    name="username"
                    disabled = {true}
                    value={this.state.user.username}
                    //onChange={this.onChangeUsername}
                    //validations={[required, vusername]}
                  />:
                  
                  <Input
                    type="text"
                    className="form-control"
                    name="username"
                    value={this.state.username}
                    onChange={this.onChangeUsername}
                    validations={[required, vusername]}
                  />}
                </div>
                
                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <Input
                    type="password"
                    className="form-control"
                    name="password"
                    value={this.state.password}
                    onChange={this.onChangePassword}
                    validations={[vpassword]}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="passwordRepeat">Repeat Password</label>
                  <Input
                    type="password"
                    className="form-control"
                    name="passwordRepeat"
                    value={this.state.passwordRepeat}
                    onChange={this.onChangePasswordRepeat}
                    validations={[vpasswordRepeat]}
                  />
                </div>
                {this.state.create &&
                  <div>
                    <label htmlFor="role">Role:</label>
                    <Select name='role' value = {this.state.role} onChange = {this.onChangeRole} validations={[required]}>
                      <option value='user'>User</option>
                      <option value='power_user'>Power User</option>
                      <option value='admin'>Administrator</option>
                    </Select>
                  </div>
                }

                {this.state.update &&
                  <div>
                    <label htmlFor="role">Role:</label>
                    <Select name='role' value = {this.roleSelected()} onChange = {this.onChangeRole} validations={[required]}>
                      <option value='user'>User</option>
                      <option value='power_user'>Power User</option>
                      <option value='admin'>Administrator</option>
                    </Select>
                  </div>
                  
                }

                <br />
                {this.state.create &&
                  <div className="form-group">
                    <button className="btn btn-primary btn-block">Create</button>
                  </div>}

                  {(!this.state.update && !this.state.create) &&
                  <div className="form-group">
                    <button className="btn btn-primary btn-block">Sign Up</button>
                  </div>
                  }

                  {this.state.update &&
                  <div className="form-group">
                    <button className="btn btn-primary btn-block">Update</button>
                  </div>}
                
              </div>
            }

            {this.state.message && 
              <div className="form-group">
                <div
                  className={
                    !this.state.successful ?
                       "alert alert-danger":""
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            }

            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
        </div>
      </div>
    );
  }
}

export default withRouter(Register)