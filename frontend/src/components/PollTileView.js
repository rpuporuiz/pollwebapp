import React, { Component, useState, useEffect } from "react";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { List, ListItem } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import { Link } from "react-router-dom";

import Poll from './Poll';

import UserService from "../services/user.service";
import AuthService from "../services/auth.service";
import PollService from "../services/poll.service";

const PollTileView = () => {
  const [polls, setPolls] = useState([])
  const [selected, setSelected] = useState(0)
  const [currentUser, setCurrentUser] = useState(AuthService.getCurrentUser())
  const [succesfull, setSuccesfull] = useState(false)
  const [content, setContent] = useState("")

  useEffect(()=>{
    updateData()
  },[])

  // useEffect(()=>{
  //   updateData()
  // },[selected])

  const updateData = () =>{
    UserService.getUserBoard().then(res => {
      setPolls(res.data)
      //setSelected(selected)
    }).catch(error => {
      setContent((error.response &&
        error.response.data &&
        error.response.data.message) ||
        error.message ||
        error.toString())
    });
  }

  const processVote = (selectedOption, id, pollIndex)=> {
    //console.log("polls:",this.state.polls)
    PollService.vote(
      selectedOption, 
      id
      ).then(res => {
        //this.updateData()
        setSuccesfull(true)
        setSelected(pollIndex)
        //console.log("props:",this.props)
        //alert(res.message)
       
        //this.forceUpdate();
    }).catch(err => {
      if (err) throw err;
    })
  }

  const hasVoted = (index) => {
    var votedIndex = null;
   if (currentUser.username ){
      polls[index].options.map((option, index) => {
        option.votes.map(vote => {
          if (vote.user === currentUser.username){
            votedIndex = index;
          }
        })
      })
    }
    return(votedIndex);
  }

  return(
    <MuiThemeProvider>
      {content ?
     <div className="container">
        <header className="jumbotron">
          <h3>{content}</h3>
        </header>
      </div> :
    <div>
        <div className="card-group text-center" style={{width: "21.5em", margin:"0 auto"}}>
              <List>
                  <span style = {{textAlign: "center"}}>
                    <Link to={"/user"} className="nav-link">
                        List View
                    </Link>
                  </span>
                <Divider />
                {polls.map(function(item, index){
                  return(
                    <div className="card">
                        <Poll 
                            data={polls[index]}
                            user={currentUser.username}
                            closed={polls[index].closed}
                            dataCb={updateData}
                            pollIndex = {index}
                            voted = {succesfull}
                            processVote = {processVote}
                            hasVoted = {hasVoted}
                        />
                    </div>
                )}, this)}
              </List>
          </div>
        </div>
      }
    </MuiThemeProvider>
  )
}

export default PollTileView

// export default class BoardUser extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       polls : [],
//       selected : -2,
//       currentUser: AuthService.getCurrentUser(),
//       succesfull: false,
//       content: ""
//     }
//     //this.hasVoted = this.hasVoted.bind(this)
//     this.updateData = this.updateData.bind(this)
//     this.processVote = this.processVote.bind(this)
    
//   }

//   componentDidMount() {
//      this.updateData()
//   }

//   updateData(){
//     UserService.getUserBoard().then(res => {
//       this.setState({
//         polls : res.data,
//         selected : this.state.selected
//       });
//     }).catch(error => {
//       this.setState({
//         content:
//           (error.response &&
//             error.response.data &&
//             error.response.data.message) ||
//           error.message ||
//           error.toString()
//       })
//     });
//   }

//   processVote(selectedOption, id){
//     //console.log("polls:",this.state.polls)
//     PollService.vote(
//       selectedOption, 
//       id
//       ).then(res => {
//         //this.updateData()
//         this.setState({
//           succesfull:true
//         })
//         //console.log("props:",this.props)
//         alert(res.message)
       
//         //this.forceUpdate();
//     }).catch(err => {
//       if (err) throw err;
//     })
  
// }

// const hasVoted = () => {
//   var votedIndex = null;
//  if (currentUser.username){
//     polls[selected].options.map((option, index) => {
//       option.votes.map(vote => {
//         if (vote.user === currentUser.username){
//           votedIndex = index;
//         }
//       })
//     })
//   }
//   return(votedIndex);
// }


//   // hasVoted(){
//   //   var votedIndex = null;
//   //  if (this.state.currentUser.username){
//   //     this.state.polls[this.state.selected].options.map((option, index) => {
//   //       option.votes.map(vote => {
//   //         if (vote.username === this.state.currentUser.username){
//   //           votedIndex = index;
//   //         }
//   //       })
//   //     })
//   //   }
//   //   return(votedIndex);
//   // }

//   render() {
//   //     const yourPolls = this.state.polls.filter((poll) => {
//   //     return (poll.author.username === this.state.currentUser.username)
//   //  })

//    return(
//     <MuiThemeProvider>
//       {this.state.content ?
//      <div className="container">
//         <header className="jumbotron">
//           <h3>{this.state.content}</h3>
//         </header>
//       </div> :
//     <div>
//         <div className="card-group">

//               <List>
//                   <span style = {{textAlign: "center"}}>
//                     <Link to={"/user"} className="nav-link">
//                         List View
//                     </Link>
//                   </span>
//                 <Divider />
//                 {this.state.polls.map(function(item, index){
//                   return(
//                     <div className="card">
//                         <Poll 
//                             data={this.state.polls[index]}
//                             user={this.state.currentUser.username}
//                             closed={this.state.polls[index].closed}
//                             dataCb={this.updateData}
//                             pollIndex = {index}
//                             voted = {this.state.succesfull}
//                             processVote = {this.processVote}
//                             hasVote = {this.hasVoted}
//                         />
//                     </div>
                    
//                 )}, this)}
//               </List>
//             </div>
         
//           </div>

// }
//     </MuiThemeProvider>
        
      
//   )
//     // return (
//     //   <div className="container">
//     //     <header className="jumbotron">
//     //       <h3>{this.state.content}</h3>
//     //     </header>
//     //   </div>
//     // );
//   }
// }