import React, { Component, useState } from 'react';
import { List, ListItem } from 'material-ui/List';

import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton';
import UserService from '../services/user.service';

const DeletePoll = (props) => {
  const [isOpen, setIsOpen] = useState(false)
  const [selectedIndex, setSelectedIndex] = useState(0)
  const [users, setUsers] = useState(props.data)
  const [userDeleted, setUserDeleted] = useState(false)
  const [initialRender, setInitialRender] = useState(true)
  const [usersNumber, setUsersNumber] = useState(props.data.lenght)
  const [content, setContent] = useState("")

  const handleOpen = (index) =>{
    setIsOpen(true)
    setSelectedIndex(index)
  };

  const handleClose = () =>{
    setIsOpen(false)
    setSelectedIndex(0)
  };

  const updateData = () =>{
    UserService.getAdminBoard().then(res => {
      setUsers(res.data)
      setUserDeleted(true)
      setIsOpen(false)
      setSelectedIndex(0)
      setUsersNumber(usersNumber - 1)
    }).catch(error => {
      let newContent = (error.response &&
        error.response.data &&
        error.response.data.message) ||
        error.message ||
        error.toString()
      
      setContent(newContent)
   });
  }

  const deleteUser = () =>{
    UserService.delete(
      users[selectedIndex]._id
    ).then(res => {
      updateData();
    }).catch(err => {
      if (err) throw err;
    })
  }

  const actions = [      
    <FlatButton
      label="Cancel"
      secondary={true}
      onClick={handleClose}
    />,
    <FlatButton
      label="Delete"
      primary={true}
      onClick={deleteUser}
    />
  ]
  let dialogText= usersNumber > 0 && "Are you sure you want to delete " + 
  this.state.users[selectedIndex].username

  return(
    <div style={{margin : 'auto', width : '80%'}}>
      <h2 style={{textAlign : 'center'}}> Select a user to delete </h2>
        <List>
          {(userDeleted || initialRender) && users.map((user, index) => {
            return(
              <div key= {user.id}>
                <ListItem
                  //disabled={true}
                  style = {{ borderBottom: "1px solid black"}}
                  key={index}
                  primaryText={user.username}
                  onClick={() => handleOpen(index)}
                  rightIcon = {
                    <span>                   
                      <i class="material-icons">delete</i>
                    </span>                 
                  }
                />
              </div>
            )
          })}
        <Dialog
            title="Delete User"
            actions={actions}
            modal={true}
            open={isOpen}
        >
          {usersNumber > 0 && "Are you sure you want to delete " + 
            users[selectedIndex].username
          }
        </Dialog>
      </List>
    </div>
  )
}

export default DeletePoll

// export default class DeletePoll extends Component{
//   constructor(props){
//     super(props);
    
//     this.state = {
//       isOpen : false,
//       selectedIndex : 0,
//       users: this.props.data,
//       userDeleted: false,
//       initialRender: true,
//       usersNumber:this.props.data.lenght
//     };
//     this.handleOpen = this.handleOpen.bind(this)
//     this.handleClose = this.handleClose.bind(this)
//     this.deleteUser = this.deleteUser.bind(this)
//   }
  
//   handleOpen(index){
//     this.setState({
//       isOpen: true,
//       selectedIndex : index
//     });
//   };

//   handleClose(){
//     this.setState({
//       isOpen: false,
//       selectedIndex : 0
//     });
//   };

//   updateData(){
//     UserService.getAdminBoard().then(res => {
//         this.setState({
//             users : res.data,
//             userDeleted: true,
//             isOpen: false,
//             selectedIndex:0,
//             usersNumber: this.state.usersNumber - 1
//       });
//     }).catch(error => {
//       this.setState({
//         content:
//           (error.response &&
//             error.response.data &&
//             error.response.data.message) ||
//           error.message ||
//           error.toString()
//       })
//     });
//   }
  
//   deleteUser(){
//     UserService.delete(
//       this.state.users[this.state.selectedIndex]._id
//     ).then(res => {
      
//       //this.handleClose()
//       //this.props.dataCb();
//       this.updateData();
//       //alert(res.message)
      
//       //this.forceUpdate()
//     }).catch(err => {
//       if (err) throw err;
//     })
//   }
  
//   render(){
//     const actions = [      
//       <FlatButton
//         label="Cancel"
//         secondary={true}
//         onClick={this.handleClose}
//       />,
//       <FlatButton
//         label="Delete"
//         primary={true}
//         onClick={this.deleteUser}
//       />
//     ]
//     let dialogText= this.state.usersNumber > 0 && "Are you sure you want to delete " + 
//     this.state.users[this.state.selectedIndex].username
  
    
//     return(
//       <div style={{margin : 'auto', width : '80%'}}>
//         <h2 style={{textAlign : 'center'}}> Select a user to delete </h2>
//         <List>
//           {(this.state.userDeleted || this.state.initialRender) && this.state.users.map((user, index) => {
//             return(
//               <div key= {user.id}>
//                 <ListItem
//                   //disabled={true}
//                   style = {{ borderBottom: "1px solid black"}}
//                   key={index}
//                   primaryText={user.username}
//                   onClick={() => this.handleOpen(index)}
//                   rightIcon = {
//                     <span>                   
//                       <i class="material-icons">delete</i>
//                     </span>                 
//                   }
//                 />
//               </div>
//             )
//           })}
//         <Dialog
//             title="Delete User"
//             actions={actions}
//             modal={true}
//             open={this.state.isOpen}
//         >
//           {this.state.usersNumber > 0 && "Are you sure you want to delete " + 
//             this.state.users[this.state.selectedIndex].username
//           }
//         </Dialog>
          
        
//         </List>
//       </div>
//     )
//   }
// }