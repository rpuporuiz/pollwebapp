import React, { Component } from "react";
import AuthService from "../services/auth.service";
import { Link } from "react-router-dom";
import utils from '../utils'

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: AuthService.getCurrentUser(),
      img: '',
    };
  }
   componentDidMount() {
    if(this.state.currentUser.avatar.data){
      this.setState({
        img: utils.imgDecode(this.state.currentUser.avatar.data)
     })
    } else {
      this.setState({
        img: "//ssl.gstatic.com/accounts/ui/avatar_2x.png"
     })
    }
   
    
  
   }

  render() {
    const { currentUser } = this.state;

    return (
         <div className="container">
          <header className="jumbotron">
            <h3>
              <strong>{currentUser.username}</strong> Profile
            </h3>
            <Link to={"/profileUpd"} className="nav-link">
                Update Profile
            </Link>
            
              <img src = {this.state.img} alt = "Profile-img" className="profile-img-card" />
            
          </header>
          
          <p>
            <strong>Token:</strong>{" "}
            {currentUser.accessToken.substring(0, 20)} ...{" "}
            {currentUser.accessToken.substr(currentUser.accessToken.length - 20)}
          </p>
          <p>
            <strong>Id:</strong>{" "}
            {currentUser.id}
          </p>
          <p>
            <strong>Full Name:</strong>{" "}{currentUser.fullName}
          </p>
          
          
          <strong>Authorities:</strong>
          <ul>
            {currentUser.roles &&
              currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
          </ul>
          
        </div>
      
    );
  }
}