import React from 'react';

const UserStats = ({data}) => {
  const totalUsers = data.length;
  let totalUserRol = 0;
  let totalPwrUserRol = 0;
  let totalAdminRol = 0;
  data.map((user) => {
    if (user.roles.length === 1) {
      totalUserRol++;
    }
    if (user.roles.length === 2) {
      totalPwrUserRol++;
    }
    if (user.roles.length === 3) {
      totalAdminRol++;
    }
  })
  return(
    <div className="statistics">
      <h2>User  Statistics</h2>
      <p><b>Total Users Registered</b></p>
      <p>{totalUsers}</p>
      <p><b>Total Users with role 'user'</b></p>
      <p> {totalUserRol}</p>
      <p><b>Total Users with role 'power_user'</b></p>
      <p>{totalPwrUserRol}</p>
      <p><b>Total Users with role 'admin'</b></p>
      <p>{totalAdminRol}</p>
    </div>
  );
}

export default UserStats