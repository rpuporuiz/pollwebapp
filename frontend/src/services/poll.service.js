import axios from 'axios';
import AuthService from './auth.service'

const API_URL ='http://localhost:8080/api/poll/';

class PollService {
 
  async createPoll(title, options) {
    let username = AuthService.getCurrentUser().username
    //console.log("pollservice",title,options,username)
    const response = await axios.post(API_URL + 'create', {
      title,
      options,
      username
    });
    return response.data;        
  }

  async vote(index, id){
    let username = AuthService.getCurrentUser().username
    const response = await axios.post(API_URL + 'vote', {
      index,
      id,
      username
    });
    return response.data;        
    }

    updatePoll(id,options,updated,closed){
      return axios.put(API_URL + 'update',{
            id,
            options,
            updated,
            closed
          }) 
    }

    async delete(id){
      const response = await axios.post(API_URL + 'delete', {
        id
      });
      return response.data; 
    }

    list(){
      return axios.get(API_URL + 'list')
    //   .then(response => {
    //     console.log(response.data)
    //     return response.data;
    //   }); 
    }
    
}

export default new PollService();