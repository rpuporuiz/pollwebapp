import axios from 'axios';
import authHeader from './auth-header';

const API_URL ='http://localhost:8080/api/test/';

class UserService {
  getPublicContent() {
    return axios.get(API_URL + 'all');
  }

  getUserBoard() {
    return axios.get(API_URL + 'user', { headers: authHeader() });
  }

  getPowerUserBoard() {
    return axios.get(API_URL + 'pow', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + 'admin', { headers: authHeader() });
  }

  delete(id){
    return axios.post(API_URL + 'delete',{
      id
    }).then(response => {
      //console.log(response.data)
      return response.data;
    }); 
  }
}

export default new UserService();