import axios from "axios";

const API_URL ="http://localhost:8080/api/auth/";

class AuthService {
    //POST {username, password} & save JWT to Local Storage
  login(username, password) {
    return axios
      .post(API_URL + "signin", {
        username,
        password
      })
      .then(response => {
        //console.log(response.data)
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }
  //remove JWT from Local Storage
  logout() {
    localStorage.removeItem("user");
  }
  //POST {username, password}
  register(username, password, roles) {
    return axios.post(API_URL + "signup", {
      username,
      password,
      roles
    });
  }

  update(user){
    //console.log("user2service: ", user)
    return axios.put(API_URL + "update", user);
  }  
  //get stored user information (including JWT)
  getCurrentUser() {
    //console.log(JSON.parse(localStorage.getItem('user')))
    return JSON.parse(localStorage.getItem('user'))

  }
}

export default new AuthService();