import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import '@fortawesome/fontawesome-free/css/all.min.css';
import "./App.css";
import 'mdbreact/dist/css/mdb.css';

import AuthService from "./services/auth.service";

import Login from "./components/Login";
import Register from "./components/Register";
import Profile from "./components/Profile";
import BoardUser from "./components/BoardUser";
import BoardPowerUser from "./components/BoardPowerUser";
import BoardAdmin from "./components/BoardAdminUser";
import ProfileUpdate from "./components/ProfileUpdate";
import CreatePoll from "./components/CreatePoll"
import PollTileView from "./components/PollTileView"

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showPowerUserBoard: false,
      showAdminBoard: false,
      currentUser: undefined
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
        showPowerUserBoard: user.roles.includes("ROLE_POWER_USER"),
        showAdminBoard: user.roles.includes("ROLE_ADMIN")
      });
    }
  }

  logOut() {
    AuthService.logout();
  }

  render() {
    const { currentUser, showPowerUserBoard, showAdminBoard } = this.state;

    return (
      <Router>
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-dark">
            {currentUser ? 
                  <Link to={"/user"} className="navbar-brand">
                    Poll App
                  </Link>:
                  <Link to={"/login"} className="navbar-brand">
                  Poll App
                </Link>
            }
            {/* {currentUser && (
                <li className="nav-item">
                  <Link to={"/user"} className="nav-link">
                    Polls
                  </Link>
                </li>
              )} */}
            <div className="navbar-nav mr-auto">
              {/* <li className="nav-item">
                {currentUser ? 
                  <Link to={"/user"} className="nav-link">
                    Polls
                  </Link>:
                  <Link to={"/signin"} className="nav-link">
                  Home
                </Link>
                  }
              </li> */}

              {showPowerUserBoard && (
                <li className="nav-item">
                  <Link to={"/pow"} className="nav-link">
                    Polls Admin
                  </Link>
                </li>
              )}

              {showAdminBoard && (
                <li className="nav-item">
                  <Link to={"/admin"} className="nav-link">
                    Users Admin
                  </Link>
                </li>
              )}

              {currentUser && (
                <li className="nav-item">
                  <Link to={"/user"} className="nav-link">
                    Polls
                  </Link>
                </li>
              )}
            </div>

            {currentUser ? (
              <div className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to={"/profile"} className="nav-link">
                    {currentUser.username}
                  </Link>
                </li>
                <li className="nav-item">
                  <a href="/login" className="nav-link" onClick={this.logOut}>
                    LogOut
                  </a>
                </li>
              </div>
            ) : (
              <div className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to={"/login"} className="nav-link">
                    Login
                  </Link>
                </li>

                <li className="nav-item">
                  <Link to={"/register"} className="nav-link">
                    Sign Up
                  </Link>
                </li>
              </div>
            )}
          </nav>

          <div className="container mt-3">
            <Switch>
              <Route exact path={["/", "/home", "/login"]} component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/profile" component={Profile} />
              <Route path="/user" component={BoardUser} />
              <Route path="/pow" component={BoardPowerUser} />
              <Route path="/admin" component={BoardAdmin} />
              <Route path="/profileUpd" component={ProfileUpdate} />
              <Route path="/createPoll" component={CreatePoll} />
              <Route path="/pollsTile" component={PollTileView} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;