exports.imgDecode = (data) =>{
    //var base64Flag = 'data:image/jpeg;base64,';
    var imageStr  = new Buffer(data, 'base64');
    return imageStr;
  }

exports.poll = [{
  date: Date.now,
  title: "Poll Title",
  allowCustom: false,
  author: {
    displayName: "Reinier",
    user: "rpupo"
  },
  options: [
    {
    label: "option1",
    votes : [
      {user: "user1"},
      {user: "user2"}
    ]
  },
  {
    label: "option2",
    votes : [
      {user: "user3"},
      {user: "user4"}
    ]
  }
] 
}]